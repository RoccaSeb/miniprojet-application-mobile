package fr.uavignon.ceri.miniprojet.data.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.miniprojet.data.Piece;

@Database(entities = {Piece.class}, version = 6, exportSchema = false)
public abstract class MuseumRoomDatabase extends RoomDatabase {

    private static final String TAG = MuseumRoomDatabase.class.getSimpleName();

    public abstract MuseumDao museumDao();

    private static MuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static MuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate
                        /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"book_database")
                                    .build();

                     */

                            // with populate
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    MuseumRoomDatabase.class,"book_database")
                                    .fallbackToDestructiveMigration()
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        MuseumDao dao = INSTANCE.museumDao();
                        dao.deleteAll();

                        /* Museum[] cities = {new Museum("Avignon", "France"),
                        new Museum("Paris", "France"),
                        new Museum("Rennes", "France"),
                        new Museum("Montreal", "Canada"),
                        new Museum("Rio de Janeiro", "Brazil"),
                        new Museum("Papeete", "French Polynesia"),
                        new Museum("Sydney", "Australia"),
                        new Museum("Seoul", "South Korea"),
                        new Museum("Bamako", "Mali"),
                        new Museum("Istanbul", "Turkey")};
                        */


                        /*for(Museum newPiece : cities)
                            dao.insert(newPiece);
                        Log.d(TAG,"database populated");*/
                    });

                }
            };



}
