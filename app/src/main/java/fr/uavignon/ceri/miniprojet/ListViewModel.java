package fr.uavignon.ceri.miniprojet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.miniprojet.data.Piece;
import fr.uavignon.ceri.miniprojet.data.CeriMuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private CeriMuseumRepository repository;
    private static LiveData<List<Piece>> allPieces;
    private MutableLiveData<Boolean> isLoading;
    private MutableLiveData<Throwable> webServiceThrowable;

    public ListViewModel (Application application) {
        super(application);
        repository = CeriMuseumRepository.get(application);
        allPieces = repository.getAllPieces();
        isLoading = repository.getIsLoadingRepo();
        webServiceThrowable = repository.getWebServiceThrowableRepo();


    }

    LiveData<List<Piece>> getAllPieces() {
        return allPieces;
    }

    public void deletePiece(String id) {
        repository.deletePiece(id);
    }

    public LiveData<Boolean> getIsLoadingModel()
    {
        return isLoading;
    }

    public LiveData<Throwable> getWebServiceThrowableModel()
    {
        return webServiceThrowable;
    }

    public void eraseWebServiceThrowableModel()
    {
        repository.eraseWebServiceThrowableRepo();
        webServiceThrowable = repository.getWebServiceThrowableRepo();
    }


    public void loadPieces()
    {
        Thread t = new Thread() {
            public void run() {
                repository.loadCollection();
            }

            ;
        };
        t.start();

    }


    //Obligé d'utiliser un thread sinon... java.lang.IllegalStateException: Cannot access database
    // on the main thread since it may potentially lock the UI for a long period of time.
    public void updateMuseumChronologicalOrder() {
        Thread t = new Thread() {
            public void run() {
                repository.getPiecesChronologicalOrder();
            }

            ;
        };
        t.start();
        this.allPieces = repository.getAllPieces();




    }

    public void updateMuseumAlphabeticalOrder() {
        Thread t = new Thread() {
            public void run() {
                repository.getPiecesAlphabeticalOrder();
            }

            ;
        };
        t.start();
        this.allPieces = repository.getAllPieces();
    }

    public void removeAll()
    {
        Thread t = new Thread() {
            public void run() {
                repository.removeAll();
            }

            ;
        };
        t.start();
    }


}
