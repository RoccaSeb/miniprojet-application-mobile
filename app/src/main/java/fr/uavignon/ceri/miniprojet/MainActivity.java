package fr.uavignon.ceri.miniprojet;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ListViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {
            viewModel.loadPieces();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Récupération des données...",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }
        if (id == R.id.action_tri_alpha)
        {

            viewModel.updateMuseumAlphabeticalOrder();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Tri par ordre alphabétique...",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }

        if (id == R.id.action_tri_chrono)
        {

            viewModel.updateMuseumChronologicalOrder();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Tri par ordre chronologique...",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }

        if(id == R.id.action_delete_all) {
            viewModel.removeAll();
            Snackbar.make(getWindow().getDecorView().getRootView()
                    , "Suppression de toutes les pièces du musée de la base de données...",
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }
}