package fr.uavignon.ceri.miniprojet.data;

import java.util.List;
import java.util.Map;

public class ItemResponse {
    public final String name = null;
    public final Boolean working = null;
    public final String  description = null;
    public final List<String> categories = null;
    public final String brand = null;
    public final Integer year = null;

    public final Map<String, String> pictures = null;

}
