package fr.uavignon.ceri.miniprojet;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textPieceName, textYear, textDescription, textHumidity, textCategories, textBrand, textIsWorking;
    private ImageView imgPiece;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String itemID = args.getCityNum();
        Log.d(TAG,"selected id="+itemID);
        viewModel.setPiece(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textPieceName = getView().findViewById(R.id.nameItem);
        textYear = getView().findViewById(R.id.year);
        textDescription = getView().findViewById(R.id.editDescription);
        //textHumidity = getView().findViewById(R.id.editHumidity);
        textCategories = getView().findViewById(R.id.editCategories);
        textBrand = getView().findViewById(R.id.editBrand);
        textIsWorking = getView().findViewById(R.id.editIsWorking);

        imgPiece = getView().findViewById(R.id.iconePiece);

        progress = getView().findViewById(R.id.progress);



        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.miniprojet.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getCurrentPiece().observe(getViewLifecycleOwner(),
                piece -> {
                    if (piece != null) {
                        Log.d(TAG, "Observing piece");

                        textPieceName.setText(piece.getName());
                        if(piece.getYear() != 0) {
                            textYear.setText("Année : " + String.valueOf(piece.getYear()));
                        }
                        else
                        {
                            textYear.setText("Année : non renseignée");
                        }
                        if (piece.getDescription() != null)
                            textDescription.setText(piece.getDescription());
                        /*f (piece.getHumidity() != null)
                            textHumidity.setText(city.getHumidity()+" %"); */
                        if (piece.getBrand() != null)
                            textBrand.setText(piece.getBrand());
                        if(piece.isWorking())
                        {
                            textIsWorking.setText("Fonctionnel");
                        }
                        else
                        {
                            textIsWorking.setText("Non Fonctionnel");
                        }

                        if(piece.getCategories() != null)
                        {
                            textCategories.setText(piece.getCategories());
                        }
                        if(piece.getPictures() != null)
                        {
                            Glide.with(this)
                                    .load(piece.getPictures()) //on charge l'image
                                    .into(imgPiece); //dans l'emplacement "itemImage" sur le layout
                        }



                        // set ImgView
                        /*if (city.getIconUri() != null)
                            imgWeather.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(city.getIconUri(),
                                null, getContext().getPackageName())));*/

                    }
                });



    }


}