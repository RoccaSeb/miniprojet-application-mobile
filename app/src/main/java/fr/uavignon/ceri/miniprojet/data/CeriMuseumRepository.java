package fr.uavignon.ceri.miniprojet.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.miniprojet.data.database.MuseumDao;
import fr.uavignon.ceri.miniprojet.data.database.MuseumRoomDatabase;
import fr.uavignon.ceri.miniprojet.data.webservice.CeriMuseumInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.miniprojet.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class CeriMuseumRepository {

    private static final String TAG = CeriMuseumRepository.class.getSimpleName();

    private LiveData<List<Piece>> allPieces;
    private MutableLiveData<Piece> selectedPiece;
    private MutableLiveData<Boolean> isLoading;
    private final CeriMuseumInterface api;
    private MutableLiveData<Throwable> webServiceThrowable;

    private MutableLiveData<ArrayList<Piece>> pieceList = new MutableLiveData<>();


    private MuseumDao museumDao;


    private static volatile CeriMuseumRepository INSTANCE;

    public synchronized static CeriMuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CeriMuseumRepository(application);
        }

        return INSTANCE;
    }

    public CeriMuseumRepository(Application application) {
        Retrofit retrofit =
                new Retrofit.Builder()
                .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build();

        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);

        museumDao = db.museumDao();
        allPieces = museumDao.getAllPieces();
        selectedPiece = new MutableLiveData<>();
        isLoading = new MutableLiveData<Boolean>();
        webServiceThrowable = new MutableLiveData<Throwable>();
        api = retrofit.create(CeriMuseumInterface.class);
    }

    public void loadCollection()
    {
        MutableLiveData<Piece> piece = new MutableLiveData<>();
        isLoading.postValue(true);
        api.getCollection().enqueue(

        new Callback<Map<String,ItemResponse>>() {
            @Override
            public void onResponse(Call<Map< String , ItemResponse >> call,
                                   Response< Map < String , ItemResponse >> response)
            {

                updatePiece(piece.getValue());

                for (String me : response.body().keySet()) {
                    Piece p = new Piece(me);
                    CollectionResult.transferInfo(response.body().get(me), p);
                    insertPiece(p);
                }

                isLoading.postValue(false);

            }

            @Override
            public void onFailure(Call<Map<String,ItemResponse>> call, Throwable t)
            {
                isLoading.postValue(false);

                Log.d("FAILURE API : ",t.getMessage());
                webServiceThrowable.postValue(t);

            }

        });
    }

    public LiveData<List<Piece>> getAllPieces() {
        return allPieces;
    }


    public MutableLiveData<Piece> getSelectedPiece() {
        return selectedPiece;
    }

    public void getPiecesChronologicalOrder()
    {
        Log.d(TAG, "Je suis ici");
        LiveData<List<Piece>> pieces = museumDao.getSynchrPiecesChronological();
        this.allPieces = pieces;
    }

    public void getPiecesAlphabeticalOrder()
    {
        LiveData<List<Piece>> pieces = museumDao.getSynchrPiecesAlphabetical();
        this.allPieces = pieces;
    }

    public long insertPiece(Piece newPiece) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return museumDao.insert(newPiece);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return res;
    }

    public long insertOrUpdatePiece(Piece newPiece) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return museumDao.insertOrUpdate(newPiece);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int updatePiece(Piece piece) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return museumDao.update(piece);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedPiece.setValue(piece);
        return res;
    }

    public void deletePiece(String id) {
        databaseWriteExecutor.execute(() -> {
            museumDao.deletePiece(id);
        });
    }

    public void getPiece(String id)  {
        Future<Piece> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return museumDao.getPieceById(id);
        });
        try {
            selectedPiece.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public MutableLiveData<Boolean> getIsLoadingRepo()
    {
        return isLoading;
    }

    public MutableLiveData<Throwable> getWebServiceThrowableRepo() {
        return webServiceThrowable;
    }

    public void eraseWebServiceThrowableRepo()
    {
        webServiceThrowable = null;
    }

    public void removeAll(){
        museumDao.deleteAll();
        pieceList.postValue(null);
    }



}
