package fr.uavignon.ceri.miniprojet;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.miniprojet.data.Piece;

public class ListFragment extends Fragment {

    public static final String TAG = ListFragment.class.getSimpleName();

    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(ListViewModel.class);
        listenerSetup();
        observerSetup();

    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setListViewModel(viewModel);
        progress = getView().findViewById(R.id.progressList);

    }

    private void observerSetup() {
        viewModel.getAllPieces().observe(getViewLifecycleOwner(),
                pieces -> adapter.setPiecesList(pieces));


        viewModel.getIsLoadingModel().observe(getViewLifecycleOwner(),

                isLoading -> {
                    if(isLoading == false) {
                        progress.setVisibility(ProgressBar.GONE);
                    }
                    else
                    {
                        progress.setVisibility(ProgressBar.VISIBLE);
                    }
                });

        viewModel.getWebServiceThrowableModel().observe(getViewLifecycleOwner(),
                webServiceThrowable -> {
                    if(webServiceThrowable != null)
                    {
                        Snackbar.make(getView(), webServiceThrowable.getMessage(),
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        viewModel.eraseWebServiceThrowableModel();
                    }
                });

    }

}