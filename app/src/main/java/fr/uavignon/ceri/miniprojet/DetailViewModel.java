package fr.uavignon.ceri.miniprojet;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.miniprojet.data.Piece;
import fr.uavignon.ceri.miniprojet.data.CeriMuseumRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private CeriMuseumRepository repository;
    private MutableLiveData<Piece> piece;

    public DetailViewModel (Application application) {
        super(application);
        repository = CeriMuseumRepository.get(application);
        piece = new MutableLiveData<>();
    }

    public void setPiece(String id) {
        repository.getPiece(id);
        piece = repository.getSelectedPiece();
    }
    LiveData<Piece> getCurrentPiece() {
        return piece;
    }
}

