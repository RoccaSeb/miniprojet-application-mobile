package fr.uavignon.ceri.miniprojet.data.webservice;

import java.util.Map;

import fr.uavignon.ceri.miniprojet.data.ItemResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface CeriMuseumInterface {
    @Headers("Accept: application/json")
    @GET("collection")
    Call <Map<String,ItemResponse>> getCollection();
}
