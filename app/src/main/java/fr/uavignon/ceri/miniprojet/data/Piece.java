package fr.uavignon.ceri.miniprojet.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Entity(tableName = "Piece", indices = {@Index(value = {"name"},
        unique = true)})
public class Piece {

    public Piece(String id) {
        this.id = id;
    };

    public static final String TAG = Piece.class.getSimpleName();

    public static final long ADD_ID = -1;

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="year")
    private int year;



    @NonNull
    @ColumnInfo
    private String brand;

    @ColumnInfo(name="categories")
    private String categories;

    @ColumnInfo(name="description")
    private String description; // description of the current weather condition (ex: light intensity drizzle)


    @ColumnInfo(name="isworking")
    private boolean working;

    @NonNull
    @ColumnInfo(name="pictures")
    private String pictures;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(int y)
    {
        this.year = y;
    }

    public int getYear() {
        return year;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription()
    {
        return this.description;
    }
}
