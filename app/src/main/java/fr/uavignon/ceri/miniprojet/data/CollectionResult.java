package fr.uavignon.ceri.miniprojet.data;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Map;

public class CollectionResult {

    private Collection collection;
    ArrayList<String> col = new ArrayList<String>();
    //private Museum museumItems;

    static void transferInfo(ItemResponse item, Piece p)
    {

            p.setName(item.name);
            p.setDescription(item.description);
            if(item.working != null) {
                if (item.working == true || item.working == false) {
                    p.setWorking(item.working);
                } else {
                    p.setWorking(false);
                }
            }
            else
                {
                    p.setWorking(false);
                }


            if(item.year != null)
            {
                p.setYear(item.year);
            }
            else
            {
                p.setYear(0);
            }

            if(item.brand != null)
            {
                p.setBrand(item.brand);
            }
            else
            {
                p.setBrand("Non renseigné");
            }

            if(item.pictures != null)
            {
                int cmpt = 0;
                for(String i : item.pictures.keySet())
                {
                    if(cmpt == 0){
                        p.setPictures("https://demo-lia.univ-avignon.fr/cerimuseum/items/" + p.id + "/images/" + i);
                    }
                    cmpt++;
                }
            }

            if(item.categories != null)
            {
                String c = "";
                for(int i = 0; i < item.categories.size(); i++)
                {
                    c = c + ", " + item.categories.get(i);
                }
                String categories = c.substring(2);
                p.setCategories(categories);
            }

    }




}
