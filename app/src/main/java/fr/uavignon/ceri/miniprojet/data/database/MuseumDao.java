package fr.uavignon.ceri.miniprojet.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.miniprojet.data.Piece;

@Dao
public interface MuseumDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Piece piece);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertOrUpdate(Piece piece);


    @Query("DELETE FROM Piece")
    void deleteAll();

    @Query("SELECT * from Piece ORDER BY name ASC")
    LiveData<List<Piece>> getAllPieces();

    @Query("DELETE FROM Piece WHERE _id = :id")
    void deletePiece(String id);

    @Query("SELECT * FROM Piece WHERE _id = :id")
    Piece getPieceById(String id);

    @Query("SELECT * FROM piece ORDER BY name ASC")
    LiveData<List<Piece>> getSynchrPiecesAlphabetical();

    @Query("SELECT * FROM piece ORDER BY year ASC")
    LiveData<List<Piece>> getSynchrPiecesChronological();

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Piece piece);
}
