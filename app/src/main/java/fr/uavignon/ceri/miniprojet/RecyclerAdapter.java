package fr.uavignon.ceri.miniprojet;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.miniprojet.data.Piece;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.miniprojet.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.miniprojet.RecyclerAdapter.class.getSimpleName();

    private List<Piece> pieceList;
    private ListViewModel listViewModel;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        if(i == 0)
        {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout, viewGroup, false);
        }
        else
        {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_layout2, viewGroup, false);
        }
        return new ViewHolder(v);
    }

    @Override
    public int getItemViewType(int pos)
    {
        return pos%2;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(pieceList.get(i).getName());
        viewHolder.itemDetail.setText("Marque : "+ pieceList.get(i).getBrand());
        if(pieceList.get(i).getYear() == 0)
        {
            viewHolder.itemYear.setText("Année : Non renseigné");
        }
        else
        {
            viewHolder.itemYear.setText("Année : " + String.valueOf(pieceList.get(i).getYear()));
        }

        if(pieceList.get(i).getPictures() != null)
        {
            Glide.with(viewHolder.itemView.getContext())
                    .load(pieceList.get(i).getPictures()) //on charge l'image
                    .into(viewHolder.itemImage); //dans l'emplacement "itemImage" sur le layout
        }

    }

    @Override
    public int getItemCount() {
        return pieceList == null ? 0 : pieceList.size();
    }

    public void setPiecesList(List<Piece> pieces) {
        pieceList = pieces;
        notifyDataSetChanged();
    }
    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }
    private void deleteItem(String id) {
        if (listViewModel != null)
            listViewModel.deletePiece(id);
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;
        ImageView itemImage;
        TextView itemYear;

         ActionMode actionMode;
        String idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);

            itemImage = itemView.findViewById(R.id.item_image);
            itemYear = itemView.findViewById(R.id.item_year);



            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                // Called when the user selects a contextual menu item
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_delete:
                            fr.uavignon.ceri.miniprojet.RecyclerAdapter.this.deleteItem(idSelectedLongClick);
                            Snackbar.make(itemView, "Item supprimé !",
                                    Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();
                            mode.finish(); // Action picked, so close the CAB
                            return true;


                        default:
                            return false;
                    }
                }

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id  = RecyclerAdapter.this.pieceList.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });


            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.pieceList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    // Start the CAB using the ActionMode.Callback defined above
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });
        }




     }

}